#include <vector>
#include <fstream>
#include <functional>
#include <stdlib.h>
#include <time.h>
#include <unordered_map>
#define DEFAULT_FIELD_LENGTH 5000000
#define DEFAULT_BUCKET_LENGTH 4
#define DEFAULT_RELOCATION_LIMIT 500
#define DEFAULT_FINGERPRINT_SIZE 8

// If we are using the human genome, 3 bil bases (1 byte per fingerprint; +3 GB memory)
// If we are using the E.coli genome, 4.6 mil bases (1 byte per fingerprint; +4.6 MB memory)
// u_int is 32-bit and as such is limited to +4.2 bil bases. No need for more.

// Variable x refers to the item itself (string, int, bool char; template T).
// Fingerprint of x is of char (single byte) and is stored within buckets themselves

typedef unsigned int u_int32;
typedef unsigned char u_int8;

template <typename T>
class CuckooFilter {
private:
	// Buckets / field
	std::unordered_map<u_int32, std::vector<u_int8>> buckets;

	// Reusing common variables to speed up function call of every insert, lookup, delete, hash, and fingerprint
	u_int32 f, i1, i2, index;
	u_int8 tag;
	unsigned long long mask = 255; // 0...011111111 (2^fingerprint_size - 1)

	// Field meta-data
	u_int32 field_length, bucket_length, relocation_limit;
	u_int8 fingerprint_size = DEFAULT_FINGERPRINT_SIZE;

	// Hash functions for different type of data
	std::hash<T> hash;
	std::hash<u_int8> hash_fingerprint;

public:

	CuckooFilter() {
		field_length = DEFAULT_FIELD_LENGTH;
		bucket_length = DEFAULT_BUCKET_LENGTH;
		relocation_limit = DEFAULT_RELOCATION_LIMIT;
		srand(time(NULL));
	}

	CuckooFilter(u_int32 field_length,
		u_int32 bucket_length = DEFAULT_BUCKET_LENGTH,
		u_int32 relocation_limit = DEFAULT_RELOCATION_LIMIT)
		: field_length(field_length), bucket_length(bucket_length), relocation_limit(relocation_limit)
	{
		srand(time(NULL));
	}

	~CuckooFilter() { }

	inline size_t hash_item(T x) { // Hash item (element)
		// Create and return hash of element x

		return hash(x) % field_length;
	}

	inline size_t hash_fp(u_int8 x) {	// Hash fingerprint
		// Create and return hash of fingerprint of element x

		return hash_fingerprint(x) % field_length;
	}

	inline u_int8 fingerprint(T x) { // Create fingerprint
		// Returns a reduced hash which is used to quickly signify/denote/tag content

		// Use 8-bit mask on a hash to create fingerprint
		return hash(x) & mask;
	}



	bool insert(T x) {
		// Inserts fingerprint of element x

		f = fingerprint(x);
		i1 = hash_item(x);
		i2 = i1 ^ hash_fp(f);

		// Min of two elements, but return index of bucket with fewer elements
		index = (buckets[i1].size() <= buckets[i2].size()) ? i1 : i2;

		// If both full
		if (buckets[index].size() == bucket_length) {
			u_int32 other_index, random;
			u_int8 tmp_fingerprint;
			for (u_int32 i = 0; i < relocation_limit; i++) {
				random = rand() % bucket_length; // Eject random element from bucket

				// Saving the one that is going to be ejected
				tmp_fingerprint = buckets[index][random];

				// Ejecting current and replacing it with new one
				buckets[index].erase(buckets[index].begin() + random);
				buckets[index].push_back(f);

				// Ejected element has two indices: index (current location / hash), and other location
				// which is acquired by: index XOR hash(fingerprint)
				other_index = index ^ hash_fp(tmp_fingerprint);
				// If the other one is full as well, continue but from the perspective of the ejected (ejected is now ejecting someone else)
				if (buckets[other_index].size() == bucket_length) {
					index = other_index;
				} else {	// If it is not full, add to an empty bucket slot and finish
					buckets[other_index].push_back(tmp_fingerprint);
					return true;
				}
			}
			// Reached relocation limit without successfully inserting an element, return 'false' to mark failure
			return false;
		} else {
			// If they are not equal and are not full,
			// just add the new element to the bucket with fewer elements
			buckets[index].push_back(f);
			return true;
		}
	}

	bool lookup(T x) {
		// Check if an element is present (be wary of false positives)

		f = fingerprint(x);
		i1 = hash_item(x);
		i2 = i1 ^ hash_fp(f);

		for (u_int8 item : buckets[i1]) {
			if (item == f)
				return true;
		}
		for (u_int8 item : buckets[i2]) {
			if (item == f)
				return true;
		}
		return false;
	}

	bool remove(T x) {
		// Remove an existing element (returns false if the element is not found)

		f = fingerprint(x);
		i1 = hash_item(x);
		i2 = i1 ^ hash_fp(f);

		for (u_int32 i = 0; i < buckets[i1].size(); i++) {
			if (buckets[i1][i] == f) {
				buckets[i1].erase(buckets[i1].begin() + i);
				return true;
			}
		}
		for (u_int32 i = 0; i < buckets[i2].size(); i++) {
			if (buckets[i2][i] == f) {
				buckets[i2].erase(buckets[i2].begin() + i);
				return true;
			}
		}
		return false;
	}
};
