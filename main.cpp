#include <iostream>
#include <string>
#include <chrono>
#include <set>
#include <fstream>
#include "string.h"

#include "src/cf.cpp"

using namespace std;
using namespace std::chrono;

string getMemory(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/self/status", "r");
    char line[128];
		string tmp;

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
						tmp = string(line);
						return tmp;
        }
    }
}



int main(int argc, char* argv[]) {
	int bucket_length = 4, relocation_limit = 500, k_mere_length = 1;
	string inputPath = "input.txt", testPath = "test.txt", outputPath = "output.txt";
  bool verbose = false, console = false, false_positive_check = false, print_results = false;

  // Flags
	for(int i = 0; i < argc; i++) {
		string arg = argv[i];
		if(arg == "--bucket-length")
			bucket_length = atoi(argv[i + 1]);
		if(arg == "--relocation-limit")
			relocation_limit = atoi(argv[i + 1]);
		if(arg == "--k-mere-length")
			k_mere_length = atoi(argv[i + 1]);
		if(arg == "--input-path")
			inputPath = argv[i + 1];
		if(arg == "--test-path")
			testPath = argv[i + 1];
		if(arg == "--output-path")
			outputPath = argv[i + 1];
    if(arg == "--verbose")
  			verbose = true;
    if(arg == "--console")
  			console = true;
    if(arg == "--false-positive-check")
  			false_positive_check = true;
    if(arg == "--print-results")
  			print_results = true;
	}

  // Open output file
	ofstream file(outputPath);

	int incorrect = 0;
	string line, genome = "";
	vector<string> test;
	set<string> genome_entries;

	// Add all the test entries to the test vector
	ifstream testFile(testPath);
	while (getline(testFile, line)) {
		test.push_back(line);
	}

	// Assign k_mere_length
	if(test.size() < 1) {
    cout << "Error with loading testing file!" << endl;
		file << "Error with loading testing file!" << endl;
		return 0;
	} else {
		k_mere_length = test[0].size();
	}

	// Construct genome
	ifstream inputFile(inputPath);
	while (getline(inputFile, line)) {
		genome += line;
	}

	// Assign k_mere_length
	if (genome.size() < 1) {
    cout << "Error with loading input file!" << endl;
		file << "Error with loading input file!" << endl;
		return 0;
	}

	// Remove duplicates
	for (int i = 0; i <= genome.size() - k_mere_length; i++) {
		genome_entries.insert(genome.substr(i, k_mere_length));
	}

	auto start = high_resolution_clock::now();

	// Construct CF structure
	CuckooFilter<string> cf = CuckooFilter<string>(genome_entries.size(), bucket_length, relocation_limit);

	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start);
  if(verbose)
	 file << "Constructing cuckoo filter: " << duration.count() / 1000000 << "." << duration.count() % 1000000 << " seconds" << endl;


	start = high_resolution_clock::now();

  // Build CF structure
	int unsuccessful_inserts = 0;
	for(string str : genome_entries) {
		if(!cf.insert(str)) {
			unsuccessful_inserts++;
		}
	}

	stop = high_resolution_clock::now();
	duration = duration_cast<microseconds>(stop - start);
  if(verbose)
	 file << "Building cuckoo filter: " << duration.count() / 1000000 << "." << duration.count() % 1000000 << " seconds" << endl;

	start = high_resolution_clock::now();

	// Test lookups
  bool false_positive = false;
  if (print_results)
     file << "Result of lookups:" << endl;
  int i = 0;
	for (string str : test) {
    bool outcome = cf.lookup(str);
    if (print_results)
			bool false_positive = false;
		if (false_positive_check && outcome != (genome.find(str) != -1)) {
			incorrect++;
      if (print_results)
  			bool false_positive = true;
		}
    if (print_results)
			file << ++i << ": " << str << " " << outcome << (false_positive ? " (FALSE POSITIVE)" : "") << endl;
	}

  stop = high_resolution_clock::now();
	duration = duration_cast<microseconds>(stop - start);

  if(console) {
    string cmd = "";
    cout << "Console is now enabled. Enter commands below." << endl;
    while(true) {
      getline(cin, line);
      if(line == "finish")
        break;

      int j = line.find(' ');
      cmd = line.substr(0, j);
      line = line.substr(j + 1);

      if(line == cmd) {
        cout << "Invalid command" << endl;
        continue;
      }
      if (cmd == "insert") {
        if(!cf.insert(line)) {
          cout << "Unsuccessful insertion of " << line << endl;
          unsuccessful_inserts++;
        } else {
          cout << "Successful insertion of " << line << endl;
        }
      } else if (cmd == "lookup") {
          cout << "Result: " << cf.lookup(line) << endl;
      }  else if (cmd == "remove") {
          bool result = cf.remove(line);
          if (result){
            cout << "Successful removal of element " << line << endl;
          } else {
            cout << "Element " << line << " either does not exist or the removal was unsuccessful" << endl;
          }
      } else {
        cout << "Unknown command" << endl;
      }
    }

  }

  if(verbose)
	 file << "Testing cuckoo filter: " << duration.count() / 1000000 << "." << duration.count() % 1000000 << " seconds" << endl;

	if(false_positive_check && verbose) {
	   file << "Incorrect: " << incorrect << " out of " << test.size() << "(" << ((double)incorrect / test.size())*100 << "%)" << endl;
	   file << "Unsuccessful inserts: " << unsuccessful_inserts << endl;
  }

	test.clear();
	genome_entries.clear();
	genome = "";
  if(verbose)
	   file << getMemory() << endl;
	file.close();

	return 0;
}
