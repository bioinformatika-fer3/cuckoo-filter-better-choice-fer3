#!/bin/sh
g++ main.cpp -std=c++11 -o main.o
# Below 30 sec
./main.o --verbose --input-path "./test/e_coli.txt" --test-path "./test/test_1000_20.txt" --output-path "output_mutated1.txt" --false-positive-check
./main.o --verbose --input-path "./test/e_coli.txt" --test-path "./test/test_1000_50.txt" --output-path "output_generated1.txt" --false-positive-check
./main.o --verbose --input-path "./test/e_coli.txt" --test-path "./test/test_1000_500.txt" --output-path "output_generated2.txt" --false-positive-check
./main.o --verbose --input-path "./test/e_coli.txt" --test-path "./test/test_100000_200.txt" --output-path "output_substring2.txt" --false-positive-check

# Above 80 sec
./main.o --verbose --input-path "./test/e_coli.txt" --test-path "./test/test_1000000_20.txt" --output-path "output_substring1.txt" --false-positive-check

# Above 230 sec
./main.o --verbose --input-path "./test/e_coli.txt" --test-path "./test/test_10000_100.txt" --output-path "output_mutated2.txt" --false-positive-check
