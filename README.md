
﻿
# Dynamic Cuckoo Filter - FER student-project variant
## Objective
The objective of this implementation is to implement a [Dynamic Cuckoo Filter](http://www.cs.cmu.edu/~binfan/papers/conext14_cuckoofilter.pdf) with [reduced relocations](https://ieeexplore.ieee.org/document/8885169). This implementation is tailored to the specific needs of the nascent field of bioinformatics. It is not intended for general use and as such should be used exclusively for data sets limited to fewer than 4.2 billion entries.
## Data Sets
Currently, any type of data is allowed as long is it is valid input in the [standard library hash function](https://en.cppreference.com/w/cpp/utility/hash).  Data set needs to be fewer than 2^32 or 4294967296 entries as we are currently using 32-bit integers to optimize memory. Namely, the reason for not using 64-bit integers is due to the fact that majority research is done on human and *E.coli* genomes which are contained within those limits. Expanding to 64-bit would more than double memory consumption and slow down the application.

This project does not implement any data compression, but can work with any hashable data (mentioned above). It is thus recommended for the user to implement data collection himself/herself and just insert the final processed entries into the application. Removing duplicates within those datasets can significantly reduce false  positive rate.

## Future changes
In future implementations, expanding to 64-bit is a distinct possibility. In addition, optimizing certain aspects such as the hash function is an option.
# Technical Information
In this section we explain everything technical in the implementation and in what way the user can interact with the application. This is intended in order to successfully receive feedback and improve in future iterations.
## Algorithm
### Concepts
Basic concepts to describe the structure in further detail:
 - Field (Buckets) - a collection of buckets. Typically an array. It is an [unordered map](https://en.cppreference.com/w/cpp/container/unordered_map) in this implemenation.
 - Buckets - a collection of fingerprints. Typically an array. It is an [vector](https://en.cppreference.com/w/cpp/container/vector) in this implemenation.
 - Fingerprint - a reduced form of hash. Typically a substring of the hash. It used to quickly check if two elements have the same contents.
 - Element (Item) - single unit of data (primitive type, class,  or other) the user is using in the structure.
 - Relocation limit - number of times elements can be relocated before the structure rejects an element insertion.
### Hash and modulo
One important thing to note here is that hash and index are used interchangeably (sometimes even *Hash reference* is used). That is due to the fact that the **index** is a **hash under a modulo operator**. The hash guarantees uniform dispersion and the modulo avoids "*index out of bounds*" errors. The module operator is determined by the max amount entries the structure can contain. It is necessary to define the maximum amount of entries in the constructor or it will default to five million.
### Complementing hashes/indices
Each element has two deterministic indices. First one is just hash of the real data of the element while the second is calculated as the following: first hash XOR hash(fingerprint(element)). Thus, the second is complementary as it can be calculated from the first. How to calculate the second from the first will be shown in the pseudo-code.
### Fingerprint
Fingerprints are a substring of the full hash. The reduction is to reduce unnecessary memory consumption but consequently incurs false positive errors. It is efficiently calculated by a mask and is often referred to as a *tag*.
### False positive rates
*False positive error* is an error in binary classification (true/false) where a function returns true for an element being present when in reality it is not present. Check the `lookup(x)` function for an example. The rate of false-positives increase exponentially as the structure is filled up with unique elements. However, the actual rate rarely exceeds 5%.

False negative errors are not present in this algorithm as noted by [this article](https://www.cs.cmu.edu/~binfan/papers/login_cuckoofilter.pdf).
### Field / Buckets data type
The data structure of the field is an [unordered_map](https://en.cppreference.com/w/cpp/container/unordered_map). The data structure of the bucket is a [vector](https://en.cppreference.com/w/cpp/container/vector) of unsigned 1 byte integers. The reason for its use is listed in the Discussion section.

## Discussion
### Reasoning Behind Decisions
#### Fingerprint sizes
Most efficient way of dynamically choosing fingerprint size is to calculate logarithm with base 10 of the max amount of entries. However, we have 4.2 billion entries and it the fingerprint size calculated this way would be around 9 bits for those entries, which is just over a byte. This requires a use of 2 bytes, with most of second byte being completely redundant.

If we instead use a logarithm with base 16 and have maximum entries, the fingerprint size will at most be 8 bits (exactly 1 byte). This is an detriment over base 10 only as we approach the maximum amount of entries, which shouldn't be an issue if we take into consideration that the human genome is around 3 billion bases (and those bases can be compressed into pairs of bases or better). On 3 billion bases (~75% occupancy), the false positive rate remains low.

Additionally, we are rounding up instead of down because we that would reduce false-positives rates. It is impossible to round up to 9 bits as on max occupancy, it would have **exactly** 8 bits.  To have two fingerprints in a single byte, it would require to have less 65536 entries, which is unlikely. Having the fingerprint be a fixed 1 byte value is also in consideration.

However, we are fixing the fingerprint size to exactly 8 bits regardless of maximum amount of entries. The reason for this is simple: we cannot take advantage of two fingerprints in a byte because it would limit the maximum amount of entries to 65536, which is very rarely going to happen and the false positive rate would be relatively high. However, if the average maximum amount of entries is in the millions, the excess bits would decrease the false positive rates as there would be less fingerprint collisions.
#### Map structure
We have decided on a map structure because it allows accessing via keys, or more specifically, fingerprints. More importantly, It has a complexity of O(1) for accessing and inserting. These two facts allow us to dynamically allocate memory instead of pre-allocating. If accuracy is paramount, a user might want to purposefully increase the maximum amount of entries beyond expected count. That will decrease the false positive rate at the cost of memory (which does not necessarily have to be expensive).
## Implementation
### Nomenclature
To better understand the following pseudo-code, the variable names are explained below:
 - CF - abbreviation for Cuckoo filter
 - x - the element in real form (primitive type, class,  or other)
 - i - index/hash of a bucket
 - f/tag - fingerprint value
 - buckets - name of the field/array which contains buckets
 - bucket_size - a constant that denotes the max number of fingerprints in a bucket
 - relocation_limit - number of relocations that can happen before the program returns a failure
 - field_length - maximum number of buckets in the structure

### Functions
The three main functions are `insert(x)`, `lookup(x)` , and `delete(x)`. Two more functions, namely `Hash(x)` and `Fingerprint(x)`, are utility functions. Each function requires the desired element to be provided.
#### Insert:
Returns true if operation was successful. Returns false if the element was rejected.
```
Insert(x):
	f=fingerprint(x)
	i1=hash(x)
	i2=i1 XOR hash(f)

	// Returns index of bucket with fewer elements
	min_i = min(buckets[i1], buckets[i2])

	// Buckets are full
	if(buckets[min_i].size == bucket_size) then
	     for(i < relocation_limit)
			tmp_fingerprint = buckets[min_i]

		     remove_at(buckets[min_i])
		     add f to buckets[min_i]

			 // Calculate the complementary index of the removed fingerprint
		     other_index = min_i XOR hash(tmp_fingerprint)
		     // The other bucket is also full
		     if (buckets[other_index].size == bucket_length) then
				 index = other_index
				 continue
		     else
			 	 // If not full, add to an empty bcuket slot and finish
			 	 add tmp_fingerprint to buckets[other_index]
				 return true

		return false
	else
	    add f to buckets[min_i]
```
#### Lookup:
Returns true if the element exists.  Returns false if it doesn't exist.
```
Lookup(x):
	f=fingerprint(x)
	i1=hash(x)
	i2=i1 XOR hash(f)

	if(bucket[i1] has f or bucket[i2] has f) then
	    return true
	else
	    return false
```
If the operation returns true but the element does not exist, it is a false-positive incident. This happens due to the fingerprint being a substring. Example:

    hash_a = hash('a') // yyyy
    hash_b = hash('b') // yyyx

	// fingerprint is substring of first 2 chars
	f_a = fingerprint(hash_a) // yy
	f_b = fingerprint(hash_b) // yy

	insert('a') // true
	lookup('b') // true, but should be false

#### Delete:
Returns true if the operation was successful. Returns false if the operation failed or it doesn't exist.
```
Delete(x):
	f=fingerprint(x)
	i1=hash(x)
	i2=i1 XOR hash(f)

	if (bucket[i1] has f) then
	   delete f
	   return true
	else if (bucket[i2] has f) then
	   delete f
	   return true
	else
	   return false
```
#### Hash:
This function uses standard library to calculate hash (Denoted by `hash_alg(x)`). However, this function returns the modulo of the calculated hash.

	Hash(x):
		hash_value = hash_alg(x);
		return hash_value % (field_length);

Under the condition that field length is a power of two, module could be replaced with bitwise-and:

	Hash(x):
		hash_value = hash_alg(x);
		return hash_value & (field_length - 1);
#### Fingerprint:
Fingerprint returns a substring of the hash. Parameter `h` is simply `Hash(x)`. To create a substring, we are simply using a mask with the bitwise-and operator. We the number `1` is shifted left, but the amount of times is equivalent to the size of the fingerprint in bits. We then reduce it by 1 to have a series of `1`.

	Fingerprint(h):
		tag = h & ((1 << fingerprint_size) - 1);
		return tag;

# Extra
### References:

 - **Article**: https://www.cs.cmu.edu/~binfan/papers/login_cuckoofilter.pdf
 - **Research paper**: http://www.cs.cmu.edu/~binfan/papers/conext14_cuckoofilter.pdf
 - **Reduced relocations**: https://ieeexplore.ieee.org/document/8885169
 - **Original CF implementation**: https://github.com/efficient/cuckoofilter/tree/master

### Developers:
 - Branimir Ivić
 - Ivan Mihovilović
